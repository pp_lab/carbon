#pragma once

#include "SBDTypePhysicalVector3.hpp"
#include "SBSStateUpdaterParticleSystem.hpp"
#include "SBDTypePhysicalMatrix33.hpp"
#include "SBAtom.hpp"
#include "cQuaternion.hpp"


class RigidObject{

public:

    RigidObject() {};
    ~RigidObject(){};
    void                                    setRigidObjectParameters(RigidObject& rb);

    /*
	SBPosition3								com;
	SBInertiaTensor33						inertiaTensor;
	SBInverseInertiaTensor33				inertiaTensorInverse;
	SBPointerIndexer<SBStructuralParticle>*	particleSystem;
	SBQuantity::mass						mass;
	Quaternion								q;
	SBPosition3								comOriginal;
	SBInertiaTensor33						inertiaTensorOriginal;
    */

    SBPosition3                             center_of_mass_;
    SBInertiaTensor33                       inertia_tensor_;
    SBInverseInertiaTensor33                inertia_tensor_inverse_;
    SBPointerIndexer<SBStructuralParticle>* particle_system_;
    SBQuantity::mass                        mass_;
    Quaternion                              q_;
    SBPosition3                             center_of_mass_original_;
    SBInertiaTensor33                       inertia_tensor_original_;

};



