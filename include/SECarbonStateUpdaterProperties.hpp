#pragma once


#include "SBGDataGraphNodeProperties.hpp" 
#include "ui_SECarbonStateUpdaterProperties.h"
#include "SBPointer.hpp"
#include "SECarbonStateUpdater.hpp"


/// This class implements the property window of the state updater

// SAMSON Element generator pro tip: add GUI functionality in this class. The non-GUI functionality should go in the SECarbonStateUpdater class

class SECarbonStateUpdaterProperties : public SBGDataGraphNodeProperties {

	SB_CLASS
	Q_OBJECT

public:

	SECarbonStateUpdaterProperties();
	virtual ~SECarbonStateUpdaterProperties();

	/// \name Identity
	//@{

	virtual SBCContainerUUID									getUUID() const;														///< Returns the widget UUID
	virtual int													getFormat() const;														///< Returns the widget name (used as a title for the embedding window)
	virtual QString												getName() const;														///< Returns the widget logo
	virtual QPixmap												getLogo() const;														///< Returns the widget format
	virtual QString												getCitation() const;													///< Returns the citation information

	//@}

	///\name Settings
	//@{

	void														loadSettings(SBGSettings *setting);										///< Loads GUI settings
	void														saveSettings(SBGSettings *setting);										///< Saves GUI settings

	//@}
	
	///\name Setup
	//@{

	virtual bool												setup();																///< Initializes the properties widget
	virtual bool												setup(SBNode* node);													///< Initializes the properties widget

	//@}

	class Observer : public SBCReferenceTarget {

		Observer(SECarbonStateUpdaterProperties* properties);
		virtual ~Observer();

		friend class SECarbonStateUpdaterProperties;

		void														onBaseEvent(SBBaseEvent* baseEvent);									///< Base event management
		void														onStateUpdaterEvent(SBStateUpdaterEvent* stateUpdaterEvent);			///< State updater event management

		SECarbonStateUpdaterProperties* properties;

	};

	public slots:

	void														onStepsChanged(int);
	void														onStepSizeChanged(double);
    void                                                        onmaxRMSDChanged(double);
    void                                                        onminRMSDChanged(double);
    void                                                        onFactorChanged(double);
    void                                                        onDecreaseFactorChanged(double);
    void                                                        onAddRBPressed();
	void														onClearPressed();
	void														onInitPressed();

private:

	friend class SECarbonStateUpdater;

	Ui::SECarbonStateUpdaterPropertiesClass							ui;
	SBPointer<SECarbonStateUpdater>									stateUpdater;

	SBPointer<Observer>											observer;
	
    //bool                                                        vectorIsClean = false; // ??

};

SB_REGISTER_TYPE(SECarbonStateUpdaterProperties, "SECarbonStateUpdaterProperties", "6EED9048-AFEC-B9B7-DF20-A17B0F6B3A2D");
