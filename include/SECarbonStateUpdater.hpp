#pragma once 

#include "SAMSON.hpp"
#include "SBDQuantityType.hpp"

#include "SBSStateUpdaterParticleSystem.hpp"
#include "SBMDynamicalModelParticleSystem.hpp"

#include "SBAtom.hpp"
#include "SBVector6.hpp"

#include "SBCReferencePointer.hpp"

#include "cRigidObject.hpp"

#include <fstream>
#include <stdio.h>

class SECarbonStateUpdater : public SBSStateUpdaterParticleSystem {

	SB_CLASS

public:

	SECarbonStateUpdater(SBMDynamicalModelParticleSystem* dynamicalModel, SBMInteractionModelParticleSystem* interactionModel);
	virtual ~SECarbonStateUpdater();
	
	/// \name Simulation
	//@{

	virtual void												updateState();
	
	//@}

	/// \name Rendering
	//@{

	virtual void												display();																///< Displays the state updater
	virtual void												displayForShadow();														///< Displays the state updater for shadow purposes
	virtual void												displayForSelection();													///< Displays the state updater for selection purposes

	virtual void												expandBounds(SBIAPosition3& bounds) const;								///< Expands the bounds to make sure the state updater fits inside them

	virtual void												collectAmbientOcclusion(const SBPosition3& boxOrigin, const SBPosition3& boxSize, unsigned int nCellsX, unsigned int nCellsY, unsigned int nCellsZ, float* ambientOcclusionData);		///< To collect ambient occlusion data

	//@}
	
// FUNCTIONS
    void                            setupRigidBodies();
    
    void                            getConnectedComponent(std::vector<SBStructuralParticle*>& selection, SBStructuralParticle* particle);
    
    void                            computeSpatialAcceleration(const SBInertia66& inertia_tensor, const SBForce6& spatial_force, SBAcceleration6& acceleration);
    
    SBForce6                        computeSpatialForce(RigidObject& rigid_body);
    
    SBMatrix33                      computeRotationMatrix(SBForce6& spatial_force, RigidObject& rigid_body, SBQuantity::length& step);
    
    void                            moveRigidObject(SBPointerIndexer<SBStructuralParticle>* particle_system, SBDTypeSpatialTransform& rigid_body_transform);
    
    SBQuantity::angstrom            computeRmsd(RigidObject& rigid_body);
    
    SBQuantity::angstrom            computeRmsd(RigidObject& rigid_body, Quaternion& quaternion, SBPosition3& dx);
    
    std::vector<int>                getProteinChainsId();
    
    SBInertiaTensor33               computeInertiaTensor(SBPointerIndexer<SBStructuralParticle>* particle_system, SBPosition3& center_of_mass);
    
    RigidObject                     getCurrentRigidObject(int chainId);
    
    void                            updateConditions();
    
    //void                            getSpatialForceArray(SBForce6* storeSpatialForceArray);
    void                            getSpatialForces(std::vector<SBForce6>& spatial_forces);
    
    void                            addRigidObject();
    
    SBQuantity::time                getStepSize(SBForce6& spatial_force, RigidObject& rigid_body);
    
    // VARIABLES
    double                          stepsize_scaling_factor_;
    double                          stepsize_decrement_;
    SBQuantity::length              max_rmsd_;
    SBQuantity::length              min_rmsd_;
    std::vector<RigidObject>*       rigid_bodies_;

    bool                           flag_initialized_ = false;
    bool                           flag_converge_ = false;

    unsigned int                   max_iterations_ = 100;

    
private:
    
    unsigned int                   converge_counter_ = 0;
    unsigned int                   counter_iterations_ = 0;
    unsigned int                   max_iterations_per_simulation = 100;
    bool                           debug_ = true;
    SBQuantity::length             max_stepsize_;
    FILE*                          output_file_;
    
    double                         spatial_force_threshold_ = 1e-2; // if force is less than this value stop
    double                         spatial_force_zero_threshold_ = 1e-10; // if force is less than this value set it to zero

    //std::vector<SBMStructuralModelConformation*> trajectory;

};

SB_REGISTER_TARGET_TYPE(SECarbonStateUpdater, "SECarbonStateUpdater", "A46C3985-5ACB-BABF-C2BC-E0038476FCC6");
SB_DECLARE_BASE_TYPE(SECarbonStateUpdater, SBSStateUpdaterParticleSystem);
