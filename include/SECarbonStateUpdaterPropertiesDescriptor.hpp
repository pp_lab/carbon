/// \headerfile SBProxy.hpp "SBProxy.hpp"
#include "SBProxy.hpp"

/// \headerfile SECarbonStateUpdaterProperties.hpp "SECarbonStateUpdaterProperties.hpp"
#include "SECarbonStateUpdaterProperties.hpp"


// Class descriptor

// SAMSON Element generator pro tip: complete this descriptor to expose this class to SAMSON and other SAMSON Elements

SB_CLASS_BEGIN(SECarbonStateUpdaterProperties);

	SB_CLASS_TYPE(SBCClass::Properties);
	SB_CLASS_DESCRIPTION("SECarbonStateUpdaterProperties : Carbon properties");

	SB_FACTORY_BEGIN;

		SB_CONSTRUCTOR_0(SECarbonStateUpdaterProperties);

	SB_FACTORY_END;

	SB_INTERFACE_BEGIN;

	SB_INTERFACE_END;

SB_CLASS_END(SECarbonStateUpdaterProperties);

