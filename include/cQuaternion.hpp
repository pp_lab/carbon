#pragma  once
#include "SBDTypePhysicalVector3.hpp"
#include "SBDQuantityType.hpp"
#include "SAMSON.hpp"

//const double TO_HALF_RAD = 3.14159265f / 360.0f;


class Quaternion
{
	//  - it's does't work in private ?
	
public:
//ctors
	Quaternion();
	Quaternion( SBQuantity::dimensionless w, SBQuantity::dimensionless x, SBQuantity::dimensionless y, SBQuantity::dimensionless z );
	void reset( );
	Quaternion inverse() const;
	Quaternion operator* (const Quaternion &b) const;
	inline bool operator== ( const Quaternion &b ) const;
	int isIdentity( ) const;
	Quaternion operator*( double s ) const;
	Quaternion operator+ ( const Quaternion& b ) const;
	SBQuantity::radian getAngle(const Quaternion& b);
	int normalize( );

	void print() {std::cout << w.toStdString() << " " << x.toStdString() << " " << y.toStdString() << " " << z.toStdString() << std::endl;};
	SBMatrix33 toMatrix();
	SBQuantity::dimensionless x, y, z, w; // this way does
};

