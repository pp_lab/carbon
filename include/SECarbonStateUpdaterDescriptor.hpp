/// \headerfile SBProxy.hpp "SBProxy.hpp"
#include "SBProxy.hpp"

/// \headerfile SECarbonStateUpdater.hpp "SECarbonStateUpdater.hpp"
#include "SECarbonStateUpdater.hpp"


// Class descriptor

// SAMSON Element generator pro tip: complete this descriptor to expose this class to SAMSON and other SAMSON Elements

SB_CLASS_BEGIN(SECarbonStateUpdater);

	SB_CLASS_TYPE(SBCClass::StateUpdaterParticleSystem);
	SB_CLASS_DESCRIPTION("SECarbonStateUpdater : Carbon description");

	SB_FACTORY_BEGIN;

		SB_CONSTRUCTOR_2(SECarbonStateUpdater, SBParticleSystem*, SBMInteractionModelParticleSystem*);

	SB_FACTORY_END;

	SB_INTERFACE_BEGIN;

	SB_INTERFACE_END;

SB_CLASS_END(SECarbonStateUpdater);

