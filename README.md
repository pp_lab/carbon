# Carbon 

This repository contains source code of the Carbon algorithm.
The complete description of Carbon you can find in [1].
Please cite [1], if you use the described algorithm in your projects.

**[1] Popov, Petr, et al. "Controlled‐advancement rigid‐body optimization of nanosystems." Journal of computational chemistry 40.27 (2019): 2391-2399.**

Please also cite [2], if you use the Docking benchmark in your projects.

[2] Popov, P., & Grudinin, S. (2015).
"Knowledge of native protein–protein interfaces is sufficient to construct predictive models for the selection of binding candidates."
Journal of chemical information and modeling, 55(10), 2242-2255.

# NOTE : The benchmarks composed to test Carbon are available at https://gitlab.com/pp_lab/carbon_benchmarks