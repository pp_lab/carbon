#include "SECarbonStateUpdaterProperties.hpp"
#include "SECarbonStateUpdater.hpp"
#include "SAMSON.hpp"
#include "SBGWindow.hpp"

SECarbonStateUpdaterProperties::SECarbonStateUpdaterProperties() {

	stateUpdater = 0;
	ui.setupUi(this);
	observer = new Observer(this);

}

SECarbonStateUpdaterProperties::~SECarbonStateUpdaterProperties() {

	if (!stateUpdater.isValid()) return;

	stateUpdater->disconnectBaseSignalFromSlot(observer(), SB_SLOT(&SECarbonStateUpdaterProperties::Observer::onBaseEvent));
	stateUpdater->disconnectStateUpdaterSignalFromSlot(observer(), SB_SLOT(&SECarbonStateUpdaterProperties::Observer::onStateUpdaterEvent));

}

void SECarbonStateUpdaterProperties::loadSettings(SBGSettings *settings) {

	if (settings == 0) return;

	ui.spinBoxSteps->setValue(settings->loadIntValue("Steps", 100));
	double v = settings->loadDoubleValue("StepSize", 1.0);
	ui.doubleSpinBoxStepSize->setValue(v);
    double ma = settings->loadDoubleValue("maxRMSD", 2.0);
    ui.doubleSpinBoxmaxRMSD->setValue(ma);
    double mi = settings->loadDoubleValue("minRMSD", 0.1);
    ui.doubleSpinBoxminRMSD->setValue(mi);
    double fact = settings->loadDoubleValue("Factor", 0.9);
    ui.doubleSpinBoxFactor->setValue(fact);
    double decFact = settings->loadDoubleValue("DecreaseFactor", 2.0);
    ui.doubleSpinBoxDecreaseFactor->setValue(decFact);
	// SAMSON Element generator pro tip: complete this function so this property window can save its GUI state from one session to the next

}

void SECarbonStateUpdaterProperties::saveSettings(SBGSettings *settings) {

	if (settings == 0) return;

	settings->saveValue("Steps", ui.spinBoxSteps->value());
	settings->saveValue("StepSize", ui.doubleSpinBoxStepSize->value());
	settings->saveValue("maxRMSD", ui.doubleSpinBoxmaxRMSD->value());
    settings->saveValue("minRMSD", ui.doubleSpinBoxminRMSD->value());
    settings->saveValue("Factor", ui.doubleSpinBoxFactor->value());
    settings->saveValue("DecreaseFactor", ui.doubleSpinBoxDecreaseFactor->value());

	// SAMSON Element generator pro tip: complete this function so this property window can save its GUI state from one session to the next

}

SBCContainerUUID SECarbonStateUpdaterProperties::getUUID() const { return SBCContainerUUID("DA06FF43-4375-7B6B-B4DB-978C2FEB3954"); }

QPixmap SECarbonStateUpdaterProperties::getLogo() const {

	// SAMSON Element generator pro tip: this icon will be visible in the GUI title bar. 
	// Modify it to better reflect the purpose of your state updater.

	return QPixmap(QString::fromStdString(SB_ELEMENT_PATH + "/Resource/Icons/SECarbonStateUpdaterPropertiesIcon.png"));

}

QString SECarbonStateUpdaterProperties::getName() const {

	// SAMSON Element generator pro tip: this string will be the GUI title. 
	// Modify this function to have a user-friendly description of your state updater inside SAMSON

	return "SECarbonStateUpdater properties window";

}

int SECarbonStateUpdaterProperties::getFormat() const {

	// SAMSON Element generator pro tip: modify these default settings to configure the window
	//
	// SBGWindow::Savable : let users save and load interface settings (implement loadSettings and saveSettings)
	// SBGWindow::Lockable : let users lock the window on top
	// SBGWindow::Resizable : let users resize the window
	// SBGWindow::Citable : let users obtain citation information (implement getCitation)

	return (SBGWindow::Savable | SBGWindow::Lockable | SBGWindow::Resizable | SBGWindow::Citable);

}

QString SECarbonStateUpdaterProperties::getCitation() const {

	// SAMSON Element generator pro tip: modify this function to add citation information

	return
		"If you use this state updater in your work, please cite: <br/>"
		"<br/>"
		"[1] <a href=\"https://www.samson-connect.net\">https://www.samson-connect.net</a><br/>";

}

void SECarbonStateUpdaterProperties::onStepsChanged(int i) { stateUpdater->setNumberOfSteps(i); stateUpdater->max_iterations_=i;}
void SECarbonStateUpdaterProperties::onStepSizeChanged(double d) { stateUpdater->setStepSize(SBQuantity::femtosecond(d)); }
void SECarbonStateUpdaterProperties::onmaxRMSDChanged(double ma) {stateUpdater->max_rmsd_ = SBQuantity::angstrom(ma);}
void SECarbonStateUpdaterProperties::onminRMSDChanged(double mi) {stateUpdater->min_rmsd_ = SBQuantity::angstrom(mi);}
void SECarbonStateUpdaterProperties::onFactorChanged(double fact) {stateUpdater->stepsize_decrement_ = fact;}
void SECarbonStateUpdaterProperties::onDecreaseFactorChanged(double decFact) {stateUpdater->stepsize_scaling_factor_ = decFact;}

bool SECarbonStateUpdaterProperties::setup() {

SBNodeIndexer nodeIndexer;
	SB_FOR(SBNode* node, *SAMSON::getActiveDocument()->getSelectedNodes()) node->getNodes(nodeIndexer, SBNode::GetClass() == std::string("SECarbonStateUpdater") && SBNode::GetElement() == std::string("SECarbon") && SBNode::GetElementUUID() == SBUUID(SB_ELEMENT_UUID));

	if (nodeIndexer.size() == 1) {

		stateUpdater = static_cast<SECarbonStateUpdater*>(nodeIndexer[0]);
		stateUpdater->connectBaseSignalToSlot(observer(), SB_SLOT(&SECarbonStateUpdaterProperties::Observer::onBaseEvent));
		stateUpdater->connectStateUpdaterSignalToSlot(observer(), SB_SLOT(&SECarbonStateUpdaterProperties::Observer::onStateUpdaterEvent));

		stateUpdater->setStepSize(SBQuantity::femtosecond(ui.doubleSpinBoxStepSize->value()));
		stateUpdater->setNumberOfSteps(ui.spinBoxSteps->value());
		stateUpdater->max_iterations_=ui.spinBoxSteps->value();
        
        stateUpdater->max_rmsd_ = SBQuantity::angstrom(ui.doubleSpinBoxmaxRMSD->value());
        stateUpdater->min_rmsd_ = SBQuantity::angstrom(ui.doubleSpinBoxminRMSD->value());
        stateUpdater->stepsize_decrement_ = ui.doubleSpinBoxFactor->value();
        stateUpdater->stepsize_scaling_factor_ = ui.doubleSpinBoxDecreaseFactor->value();

		return true;

	}

	return false;

}

bool SECarbonStateUpdaterProperties::setup(SBNode* node) {

	if (node->getProxy()->getName() != "SECarbonStateUpdater") return false;
	if (node->getProxy()->getElement() != "SECarbon") return false;
	if (node->getProxy()->getElementUUID() != SBUUID(SB_ELEMENT_UUID)) return false;

	stateUpdater = static_cast<SECarbonStateUpdater*>(node);
	stateUpdater->connectBaseSignalToSlot(observer(), SB_SLOT(&SECarbonStateUpdaterProperties::Observer::onBaseEvent));
	stateUpdater->connectStateUpdaterSignalToSlot(observer(), SB_SLOT(&SECarbonStateUpdaterProperties::Observer::onStateUpdaterEvent));

	stateUpdater->setStepSize(SBQuantity::femtosecond(ui.doubleSpinBoxStepSize->value()));
	stateUpdater->setNumberOfSteps(ui.spinBoxSteps->value());
    stateUpdater->max_rmsd_ = SBQuantity::angstrom(ui.doubleSpinBoxmaxRMSD->value());
    stateUpdater->min_rmsd_ = SBQuantity::angstrom(ui.doubleSpinBoxminRMSD->value());
    stateUpdater->stepsize_decrement_ = ui.doubleSpinBoxFactor->value();
    stateUpdater->stepsize_scaling_factor_ = ui.doubleSpinBoxDecreaseFactor->value();
	stateUpdater->max_iterations_=ui.spinBoxSteps->value();

	return true;

}


SECarbonStateUpdaterProperties::Observer::Observer(SECarbonStateUpdaterProperties* properties) { this->properties = properties; }
SECarbonStateUpdaterProperties::Observer::~Observer() {}

void SECarbonStateUpdaterProperties::Observer::onBaseEvent(SBBaseEvent* baseEvent) {

	if (baseEvent->getType() == SBBaseEvent::NodeEraseBegin) properties->hide();
	
}

void SECarbonStateUpdaterProperties::Observer::onStateUpdaterEvent(SBStateUpdaterEvent* stateUpdaterEvent) {

	if (stateUpdaterEvent->getType() == SBStateUpdaterEvent::StateUpdaterChanged) {

	}

}

void SECarbonStateUpdaterProperties::onAddRBPressed(){
        
    stateUpdater->addRigidObject();

	return;
}

void SECarbonStateUpdaterProperties::onInitPressed(){
    
	if (stateUpdater->rigid_bodies_->size()<2) {
		SAMSON::setStatusMessage(QString("Properties : Add at least two rigid bodies and press init"), 0);
		return;
	}

	SAMSON::setStatusMessage(QString("Initialization is completed. Proceed with run."), 0);
	stateUpdater->flag_initialized_=true;
	stateUpdater->flag_converge_ = false;
	stateUpdater->stepsize_scaling_factor_=stateUpdater->rigid_bodies_->size();

	return;
}

void SECarbonStateUpdaterProperties::onClearPressed(){
    
	stateUpdater->rigid_bodies_->clear();
	stateUpdater->flag_initialized_=false;
	stateUpdater->flag_converge_ = false;
	
	return;
}