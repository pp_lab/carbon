#include "cRigidObject.hpp"


void RigidObject::setRigidObjectParameters(RigidObject& rb) {

    this->center_of_mass_ = rb.center_of_mass_;
    this->inertia_tensor_ = rb.inertia_tensor_;
    this->inertia_tensor_inverse_ = rb.inertia_tensor_inverse_;
    this->particle_system_ = rb.particle_system_;
    this->mass_ = rb.mass_;
    this->q_ = rb.q_;
    this->center_of_mass_original_ = rb.center_of_mass_original_;
    this->inertia_tensor_original_ = rb.inertia_tensor_original_;

    return;

}