#include "cQuaternion.hpp"

//ctors

Quaternion::Quaternion() {}
Quaternion::Quaternion( SBQuantity::dimensionless W, SBQuantity::dimensionless X, SBQuantity::dimensionless Y, SBQuantity::dimensionless Z ) : x(X), y(Y), z(Z), w(W)
	{
	}

void Quaternion::reset()
	{
		x = SBQuantity::dimensionless(0.0);
		y = SBQuantity::dimensionless(0.0);
		z = SBQuantity::dimensionless(0.0);
		w = SBQuantity::dimensionless(1.0);
	}


Quaternion Quaternion::inverse() const
	{
		return Quaternion( w, -x, -y, -z );
	}


Quaternion Quaternion::operator* (const Quaternion &b) const
	{
		Quaternion r;

		r.w = w*b.w - x*b.x  -  y*b.y  -  z*b.z;
		r.x = w*b.x + x*b.w  +  y*b.z  -  z*b.y;
		r.y = w*b.y + y*b.w  +  z*b.x  -  x*b.z;
		r.z = w*b.z + z*b.w  +  x*b.y  -  y*b.x;

		return r;
	}

inline bool Quaternion::operator== ( const Quaternion &b ) const
	{
		return (w == b.w && x == b.x && y == b.y && z == b.z );
	}


int Quaternion::isIdentity() const
	{
		return (w == SBQuantity::dimensionless(1.0) && x == SBQuantity::dimensionless(0.0) && y == SBQuantity::dimensionless(0.0) && z == SBQuantity::dimensionless(0.0));
	}


inline Quaternion Quaternion::operator*( double s ) const
	{
		return Quaternion(w * s, x * s, y * s, z * s  );
	}

	// Addition
Quaternion Quaternion::operator+ ( const Quaternion& b ) const
	{
		return Quaternion( w + b.w, x + b.x, y + b.y, z + b.z );
	}

int Quaternion::normalize( )
	{
		SBQuantity::dimensionless lengthSq = sqrt(x * x + y * y + z * z + w * w);

		if (lengthSq == 0.0 ) return -1;
		if (lengthSq != 1.0 )
		{
			SBQuantity::dimensionless scale = ( 1.0 /  lengthSq  );
			x *= scale;
			y *= scale;
			z *= scale;
			w *= scale;
			return 1;
		}
		return 0;
	}
SBMatrix33 Quaternion::toMatrix()
	{

		SBMatrix33 Rotation;
		Rotation.m[0][0] = 1 - 2*y*y - 2*z*z;
		Rotation.m[1][1] = 1 - 2*x*x - 2*z*z;
		Rotation.m[2][2] = 1 - 2*y*y - 2*x*x;

		Rotation.m[0][1] = 2*(x*y - w*z);
		Rotation.m[0][2] = 2*(x*z + w*y);
		Rotation.m[1][0] = 2*(x*y + w*z);
		Rotation.m[1][2] = 2*(z*y - w*x);
		Rotation.m[2][0] = 2*(x*z - w*y);
		Rotation.m[2][1] = 2*(z*y + w*x);

		return Rotation;

	}

SBQuantity::radian Quaternion::getAngle(const Quaternion& b)
{
	SBQuantity::radian angle = 2*acos(b.w);
	return angle;
}

