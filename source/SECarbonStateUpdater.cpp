#include "SECarbonStateUpdater.hpp"
#include "SECarbonStateUpdater.hpp"
#include "SBDTypePhysicalVector3.hpp"
#include "SECarbonStateUpdaterProperties.hpp"
#include "SBDQuantityType.hpp"

// TODO:
// +1) rename while counter
// +2) add maximum number of steps
// +3) fix rigid bodies initialization and handle number of rigid bodies, scaling factor, etc +
// +4) check energy values
//  5) save trajectory as a stack of conformations
// +6) check algorithm itself +
//  7) 2 rigid body mode to move only one molecule
//  8) test on different systems and picture production 

SECarbonStateUpdater::SECarbonStateUpdater(SBMDynamicalModelParticleSystem* d, SBMInteractionModelParticleSystem* i) : SBSStateUpdaterParticleSystem(d, i) {

	// SAMSON Element generator pro tip: this constructor is called when initializing the state updater.

	setName("Carbon constructor");

	//setStepSize(SBQuantity::femtosecond(1.0));
	//setNumberOfSteps(1);

    rigid_bodies_ = new std::vector<RigidObject>;
    //flag_initialized_ = false;
    debug_ = false;
}

SECarbonStateUpdater::~SECarbonStateUpdater() {

	    if (rigid_bodies_) delete rigid_bodies_;

}

void SECarbonStateUpdater::updateState() {

	// SAMSON Element generator pro tip: this is the main function of the state updater. The example below implements the steepest descent algorithm.
	// Please refer to the SAMSON SDK documentation for more information.
	// 
	// Example code: basic implementation of the steepest descent minimization algorithm
	//
	//	SBPointerIndex<SBStructuralParticle> const* particleIndex = (*dynamicalModel)->getStructuralParticleIndex();
	//
	//	unsigned int nParticles = particleIndex->size(); // number of particles in the particle system
	//	unsigned int nSteps = getNumberOfSteps(); // the number of iterations per interactive simulation step
	//
	//	for (unsigned int k = 0; k < nSteps; k++) {
	//
	//		(*interactionModel)->updateInteractions(); // compute the new energy and forces based on the updated positions
	//		(*dynamicalModel)->flushPositionBuffer(); // tell the dynamical model that we have used the list of updated positions	
	//
	//		for (unsigned int i = 0; i < nParticles; i++) {
	//
	//			SBPosition3 currentPosition = (*dynamicalModel)->getPosition(i); // get the current position of particle i
	//			SBForce3 force = (*interactionModel)->getForce(i); // get the force applied to particle i
	//			SBPosition3 newPosition = currentPosition + 1.0 / SBQuantity::mass(1)*getStepSize()*getStepSize()*force; // compute the new position (take care of dimensions)
	//			(*dynamicalModel)->setPosition(i, newPosition); // set the new position of particle i
	//
	//		}
	//
	//		(*interactionModel)->flushForceBuffer(); // notify the interaction model that we have used the list of updated forces (actually, we used all forces in this non-adaptive algorithm)
	//
	//	}
	//
	//	(*dynamicalModel)->updateStructuralNodes(); // update the structural model based on the new positions in the dynamical model


    if (flag_converge_) {
        //SAMSON::setStatusMessage(QString("\tRefinement is finished."), 0);
        return;
    }

    if (flag_initialized_!=true) {
        setupRigidBodies();
        return;
    }

    //if (counter_iterations_>=max_iterations_) {
    //    SAMSON::setStatusMessage(QString("\tMaximum number of iterations is achieved."), 0);
    //    printf("\tCarbon : Maximum number of iterations is achieved.\n");
    //    return;
    //}

    int n_rigid_bodies = rigid_bodies_->size();
    
    SBQuantity::energy energy = (*interactionModel)->getEnergy();
    SBQuantity::energy energy_new = energy;
    
    //SBForce6 SpatialForceArray[rigid_bodies_->size()];
    std::vector<SBForce6> spatial_forces;
    spatial_forces.resize(n_rigid_bodies);
    
    max_stepsize_ = max_rmsd_/stepsize_scaling_factor_;
    
    ++counter_iterations_;
    //printf("\tCarbon : Iteration %6d\n", counter_iterations_);

    
    int whileCounter = 0; // TODO: ZZZ
    
    while (energy_new >= energy){
    //for (counter_iterations_=0; counter_iterations_<=max_iterations_; ++counter_iterations_) {
    //for (int i_step=0; i_step<1; ++i_step) {
        
        //whileCounter++;
        
        if(flag_converge_){
            SAMSON::setStatusMessage(QString("Refinement is finished"), 0);
            printf("\tCarbon : Refinement is finished\n");
            break;
        }

        if (counter_iterations_==max_iterations_) {
            SAMSON::setStatusMessage(QString("\tMaximum number of iterations is achieved."), 0);
            printf("\tCarbon : Maximum number of iterations is achieved.\n");
            flag_converge_=true;
            break;
        }

        if (max_stepsize_ < min_rmsd_){
            //updateConditions();
            SAMSON::setStatusMessage(QString("\tMinimum stepsize is achieved."), 0);
            printf("\tCarbon : Minimum stepsize is achieved.");
            flag_converge_ = true;
            break;
        }
        
        
        //RigidObjectClass storeRigidObjectArray[rigid_bodies_->size()];
        //SBDTypeSpatialTransform storeTransformArray[rigid_bodies_->size()];
        
        std::vector<RigidObject> rigid_bodies; // local vector to store original conformations
        rigid_bodies.resize(n_rigid_bodies);
        
        std::vector<SBDTypeSpatialTransform> spatial_transforms;
        spatial_transforms.resize(n_rigid_bodies);
        
        if(max_stepsize_ == max_rmsd_/stepsize_scaling_factor_){
            //getSpatialForceArray(storeSpatialForceArray);
            getSpatialForces(spatial_forces);
        }
        
        unsigned int rigid_body_id = 0;

        for (std::vector<RigidObject>::iterator i_rb = rigid_bodies_->begin(); i_rb != rigid_bodies_->end(); ++i_rb) {
            
            rigid_bodies[rigid_body_id].setRigidObjectParameters((*i_rb));

            //RigidObject current_rigid_body = (*i_rb);
            //rigid_bodies[rigid_body_id] = (*i);

            //SBPointerIndexer<SBStructuralParticle>* particle_system = current_rigid_body.particle_system_;
            SBPointerIndexer<SBStructuralParticle>* particle_system = (*i_rb).particle_system_;
            
            if (debug_ == true) {
                printf("CARBON DEBUG MODE: entering cycle\n");
                printf("CARBON DEBUG MODE: size of rigid body is %d\n", particle_system->size());
            }
            
            SBForce6 spatial_force = spatial_forces[rigid_body_id];
            
            SBPosition3 dx;
            dx = spatial_force.linear/spatial_force.linear.norm()*max_stepsize_;
            
            Quaternion quaternion;
            //quaternion = current_rigid_body.q_;
            quaternion = (*i_rb).q_;
            
            SBMatrix33 rotation_matrix;
            //rotation_matrix = computeRotationMatrix(spatial_force, current_rigid_body, max_stepsize_);
            rotation_matrix = computeRotationMatrix(spatial_force, (*i_rb), max_stepsize_);

            // computing stepSize here
            SBQuantity::time dt;
            //dt = getStepSize(spatial_force, current_rigid_body);
            dt = getStepSize(spatial_force, (*i_rb));

            // checking force condition here
            if (spatial_force.linear.norm().getValue() < spatial_force_threshold_) {
                dx.setZero();
                rotation_matrix.setIdentity();
            }
            
            if (debug_ == true) {
                printf("CARBON DEBUG MODE: the parameters for (iteration %d cycle %d ; rigid body %d) are:\n", counter_iterations_, counter_iterations_, rigid_body_id);
                printf("CARBON DEBUG MODE: the net force is %.6f\t%.6f\t%.6f\n", spatial_force.linear.v[0].getValue(), spatial_force.linear.v[1].getValue(), spatial_force.linear.v[2].getValue());
                printf("CARBON DEBUG MODE: the translation is: %lf\n", dx.norm().getValue());
            }
            
            SBDTypeSpatialTransform spatial_transform(rotation_matrix, dx);
            
            //storeTransformArray[rigid_body_id] = rigidBodyTransform;
            spatial_transforms[rigid_body_id] = spatial_transform;
            
            //moveRigidObject(particle_system, spatial_transform);
            moveRigidObject( (*i_rb).particle_system_, spatial_transform);
            
            //current_rigid_body.center_of_mass_ += dx;
            //current_rigid_body.inertia_tensor_ = rotation_matrix * current_rigid_body.inertia_tensor_ * rotation_matrix.transpose();
            //current_rigid_body.inertia_tensor_inverse_ = rotation_matrix * current_rigid_body.inertia_tensor_inverse_ * rotation_matrix.transpose();
            (*i_rb).center_of_mass_ += dx;
            (*i_rb).inertia_tensor_ = rotation_matrix * (*i_rb).inertia_tensor_ * rotation_matrix.transpose();
            (*i_rb).inertia_tensor_inverse_ = rotation_matrix * (*i_rb).inertia_tensor_inverse_ * rotation_matrix.transpose();
            
            //storeRigidObjectArray[rigid_body_id] = current_rigid_body;
            //rigid_bodies_[rigid_body_id] = current_rigid_body;
            
            ++rigid_body_id;
        }
        
        updateConditions();
        energy_new = (*interactionModel)->getEnergy();
        
        if (debug_ == true) {
            printf("CARBON DEBUG MODE: body is moved\n");
            printf("CARBON DEBUG MODE: The before energy is %.16f zJ\n", energy.getValue());
            printf("CARBON DEBUG MODE: The after energy  is %.16f zJ\n", energy_new.getValue());
        }
        
        if(energy_new >= energy){
            // restore previous conformations
            unsigned int rigid_body_id = 0;
            for (std::vector<RigidObject>::iterator i_rb = rigid_bodies_->begin(); i_rb != rigid_bodies_->end(); ++i_rb) {
                
                //RigidObject current_rigid_body = (*i);
                //SBPointerIndexer<SBStructuralParticle>* particle_system = current_rigid_body.particle_system_;
                SBDTypeSpatialTransform inverse_transform = spatial_transforms[rigid_body_id].inverse();
                //moveRigidObject(particle_system, inverse_transform);
                (*i_rb).setRigidObjectParameters(rigid_bodies[rigid_body_id]); // set parameters of the rigid_body;
                moveRigidObject((*i_rb).particle_system_, inverse_transform); // move particles back
                
                ++rigid_body_id;
            }
            // decrease stepsize
            max_stepsize_ *= stepsize_decrement_;
        }
        
        else{
            //printf("\t\tIteration|Step : %8d|%8d\n", counter_iterations_, counter_iterations_);
            //printf("\t\tIteration|Step : %8d\n", counter_iterations_);
            printf("\tCarbon : Iteration|E_i|E_i-1|Stepsize : %16d | %16.4f | %16.4f | %16.4f\n", counter_iterations_, energy_new.getValue(), energy.getValue(), max_stepsize_.getValue());

            // accept current conformations
            //unsigned int rigid_body_id = 0;
            
            // why if we are already moved rb?
            /*
            for (std::vector<RigidObject>::iterator i = rigid_bodies_->begin(); i != rigid_bodies_->end(); ++i) {
                (*i) = rigid_bodies[rigid_body_id];
                rigid_body_id++;
            }
            */
            
            max_stepsize_ = max_rmsd_/stepsize_scaling_factor_;

            // add conformation to the conformation stack
            /*
            int i_conformation = trajectory_.size();
            SBMStructuralModelConformation* conformation;
            conformation =  new SBMStructuralModelConformation( "Conformation_" + QString::number(i_conformation+1).toStdString(), nodeIndexer);
            trajectory_.push_back(conformation);
            trajectory_[i_conformation]->create();
            SAMSON::getActiveDocument()->addChild(trajectory_[i_conformation]);
            */
            
            if(debug_ == true) {
                printf("DEBUG MODE storing trajectory");
                std::ostringstream fileName;
                fileName << counter_iterations_;
                std::string pdbFile = "./model_"+fileName.str()+".pdb";
                SBList<std::string>* parameters = 0;
                //SBNodeIndexer nodeIndexer;
                //SAMSON::getActiveDocument()->getNodes(nodeIndexer);
                //SAMSON::exportToFile(nodeIndexer, pdbFile, parameters);
            }
        }
        
    }
    
    if(debug_ == true){
        printf("DEBUG MODE cycle %d is finished\n", whileCounter);
    }

}

void SECarbonStateUpdater::display() {

	// SAMSON Element generator pro tip: this function is called by SAMSON during the main rendering loop. 
	// Implement this function to display things in SAMSON, for example thanks to the utility functions provided by SAMSON (e.g. displaySpheres, displayTriangles, etc.)

}

void SECarbonStateUpdater::displayForShadow() {

	// SAMSON Element generator pro tip: this function is called by SAMSON during the main rendering loop in order to compute shadows. 
	// Implement this function if your state updater displays things in viewports, so that your state updater can cast shadows
	// to other objects in SAMSON, for example thanks to the utility
	// functions provided by SAMSON (e.g. displaySpheres, displayTriangles, etc.)

}

void SECarbonStateUpdater::displayForSelection() {

	// SAMSON Element generator pro tip: this function is called by SAMSON during the main rendering loop in order to perform object picking.
	// Instead of rendering colors, your state updater is expected to write the index of a data graph node (obtained with getIndex()).
	// Implement this function so that your state updater can be selected (if you render its own index) or can be used to select other objects (if you render 
	// the other objects' indices), for example thanks to the utility functions provided by SAMSON (e.g. displaySpheresSelection, displayTrianglesSelection, etc.)
	// This should be implemented if your state updater displays something in viewports (and you want the user to be able to select your state updater
	// by picking its visual representation in viewports). 

}

void SECarbonStateUpdater::expandBounds(SBIAPosition3& bounds) const {

	// SAMSON Element generator pro tip: this function is called by SAMSON to determine the model's spatial bounds. 
	// When this function returns, the bounds interval vector should contain the state updater. 
	// This should be implemented if your state updater displays something in viewports. 

}

void SECarbonStateUpdater::collectAmbientOcclusion(const SBPosition3& boxOrigin, const SBPosition3& boxSize, unsigned int nCellsX, unsigned int nCellsY, unsigned int nCellsZ, float* ambientOcclusionData) {

	// SAMSON Element generator pro tip: this function is called by SAMSON to determine your model's influence on ambient occlusion (in case your state updater displays something in viewports).
	// Implement this function if you want your state updater to occlude other objects in ambient occlusion calculations.
	//
	// The ambientOcclusionData represents a nCellsX x nCellsY x nCellsZ grid of occlusion densities over the spatial region (boxOrigin, boxSize).
	// If your model represents geometry at position (x, y, z), then the occlusion density in corresponding grid nodes should be increased.
	//
	// Assuming x, y and z are given in length units (SBQuantity::length, SBQuantity::angstrom, etc.), the grid coordinates are:
	// SBQuantity::dimensionless xGrid = nCellsX * (x - boxOrigin.v[0]) / boxSize.v[0];
	// SBQuantity::dimensionless yGrid = nCellsY * (x - boxOrigin.v[1]) / boxSize.v[1];
	// SBQuantity::dimensionless zGrid = nCellsZ * (x - boxOrigin.v[2]) / boxSize.v[2];
	//
	// The corresponding density can be found at ambientOcclusionData[((int)zGrid.getValue() + 0)*nCellsY*nCellsX + ((int)yGrid.getValue() + 0)*nCellsX + ((int)xGrid.getValue() + 0)] (beware of grid bounds).
	// For higher-quality results, the influence of a point can be spread over neighboring grid nodes.

}

/*
void SECarbonStateUpdater::setupRigidBodies() {
    
    rigid_bodies_->clear();
    
    std::vector<int> protein_chain_ids = getProteinChainsId();
    
    if (debug_ == true){
        printf("DEBUG MODE the number of rigid bodies is %d\n", protein_chain_ids.size());
    }
    
    for (std::vector<int>::const_iterator j = protein_chain_ids.begin(); j != protein_chain_ids.end(); ++j){
        
        RigidObject current_rigid_body = getCurrentRigidObject((*j));
        rigid_bodies_->push_back(current_rigid_body);
        
        if (debug_ == true){
            printf("DEBUG MODE The size of RB %d is %d\n", (*j), current_rigid_body.particle_system_->size());
            printf("DEBUG MODE The Mass of RB%d is %f\n", (*j), current_rigid_body.mass_.getValue());
        }
    }
}
*/


void SECarbonStateUpdater::setupRigidBodies() {
    
    rigid_bodies_->clear();

    while(flag_initialized_!=true) {
        if (rigid_bodies_->size()<2) {
            SAMSON::setStatusMessage(QString("Updater : Add at least two rigid bodies and press init"), 0);
        }
        else {
            char stmp[100];
            sprintf(stmp, "Updater : There are %d rigid bodies. Press init to finish initialization", rigid_bodies_->size());
            SAMSON::setStatusMessage(QString(stmp), 0);
        }
    }

    return;   
}


SBForce6 SECarbonStateUpdater::computeSpatialForce(RigidObject& rigid_body){
    
    unsigned int n_particles = rigid_body.particle_system_->size();
    SBForce6 spatial_force = SBForce6::zero;
    
    for (unsigned int j = 0; j < n_particles; j++) {
        SBStructuralParticle* particle = rigid_body.particle_system_->getReferenceTarget(j);
        
        unsigned int particle_index_in_dynamical_model = (*dynamicalModel)->getStructuralParticleIndexer()->getIndex(particle);
        
        const SBPosition3& particle_position = (*dynamicalModel)->getPosition(particle_index_in_dynamical_model);
        
        SBForce3 particle_force = (*interactionModel)->getForce(particle_index_in_dynamical_model);
        
        SBForce6 particle_spatial_force((particle_position - rigid_body.center_of_mass_) ^ particle_force, particle_force);
        
        spatial_force += particle_spatial_force;
    }
    
    return spatial_force;
}

SBMatrix33 SECarbonStateUpdater::computeRotationMatrix(SBForce6& spatial_force, RigidObject& rigid_body, SBQuantity::length& step){
    
    SBMatrix33 rotation_matrix;
    SBQuantity::time dt = sqrt(2*step*rigid_body.mass_/spatial_force.linear.norm());
    
    if (spatial_force.linear.norm().getValue() < spatial_force_zero_threshold_){
        dt = SBQuantity::femtosecond(0.0);
    }
    
    SBPhysicalVector3<SBQuantity::angularVelocity> angular_velocity;
    angular_velocity = rigid_body.inertia_tensor_inverse_ * spatial_force.angular * dt;
    
    SBPhysicalVector3<SBQuantity::dimensionless> quaternion_axis(rigid_body.q_.x, rigid_body.q_.y, rigid_body.q_.z );
    
    Quaternion quaternion_inverse(rigid_body.q_.w, -rigid_body.q_.x, -rigid_body.q_.y, -rigid_body.q_.z);
    
    SBQuantity::dimensionless quaternion_angle = rigid_body.q_.w;
    
    quaternion_angle -= 0.5 * dt * (angular_velocity|quaternion_axis) ;
    
    quaternion_axis += angular_velocity * 0.5 * dt * rigid_body.q_.w + (angular_velocity^quaternion_axis) * dt * 0.5;
    
    rigid_body.q_.w = quaternion_angle;
    rigid_body.q_.x = quaternion_axis.v[0];
    rigid_body.q_.y = quaternion_axis.v[1];
    rigid_body.q_.z = quaternion_axis.v[2];
    rigid_body.q_.normalize();
    
    Quaternion quaternion_relative = rigid_body.q_*quaternion_inverse;
    
    SBQuantity::radian angle = 2*acos(quaternion_relative.w);
    
    rotation_matrix = quaternion_relative.toMatrix();
    
    return rotation_matrix;
}

void SECarbonStateUpdater::moveRigidObject(SBPointerIndexer<SBStructuralParticle>* particle_system, SBDTypeSpatialTransform& spatial_transform){
    
    unsigned int n_particles = particle_system->size();
    for (unsigned int j = 0; j < n_particles; j++) {
        
        SBStructuralParticle* particle = particle_system->getReferenceTarget(j);
        
        unsigned int particle_index_in_dynamical_model = (*dynamicalModel)->getStructuralParticleIndexer()->getIndex(particle);
        
        const SBPosition3& particle_position = (*dynamicalModel)->getPosition(particle_index_in_dynamical_model);
        
        (*dynamicalModel)->setPosition(particle_index_in_dynamical_model, spatial_transform*particle_position);
    }
}

SBQuantity::angstrom SECarbonStateUpdater::computeRmsd(RigidObject& rigid_body){
    
    SBPosition3 translation = rigid_body.center_of_mass_ - rigid_body.center_of_mass_original_;
    
    SBMatrix33 rotation_matrix_1;
    rotation_matrix_1.setIdentity();
    
    Quaternion quaternion = rigid_body.q_;
    SBMatrix33 rotation_matrix_2 = quaternion.toMatrix();

    SBPhysicalVector3<SBQuantity::dimensionless> quaternion_axis;
    quaternion_axis.v[0] = quaternion.x;
    quaternion_axis.v[1] = quaternion.y;
    quaternion_axis.v[2] = quaternion.z;
    
    SBQuantity::squareLength rmsd_squared;
    rmsd_squared = (quaternion_axis|(rigid_body.inertia_tensor_original_*quaternion_axis))*4/rigid_body.mass_;
    rmsd_squared += (translation)|(translation);
    rmsd_squared += 2*(translation)|((rotation_matrix_2-rotation_matrix_1)*rigid_body.center_of_mass_original_);
    
    return sqrt(rmsd_squared);
}

SBQuantity::angstrom SECarbonStateUpdater::computeRmsd(RigidObject& rigid_body, Quaternion& quaternion, SBPosition3& dx){
    
    SBQuantity::squareLength rmsd_squared;
    
    Quaternion quaternion_inverse(rigid_body.q_.w, -rigid_body.q_.x, -rigid_body.q_.y, -rigid_body.q_.z);
    
    Quaternion quaternion_relative = quaternion_inverse*quaternion;
    
    SBVector3 quaternion_relative_axis(quaternion_relative.x, quaternion_relative.y, quaternion_relative.z);
    
    rmsd_squared = dx|dx;
    rmsd_squared += 4/rigid_body.mass_*(quaternion_relative_axis|(rigid_body.inertia_tensor_original_*quaternion_relative_axis));
    rmsd_squared += 2*(dx|((quaternion.toMatrix() - rigid_body.q_.toMatrix())*rigid_body.center_of_mass_original_));
    
    return sqrt(rmsd_squared);
}

std::vector<int> SECarbonStateUpdater::getProteinChainsId(){
    
    SBPointerIndexer<SBStructuralParticle> const* structural_particle_index = (*dynamicalModel)->getStructuralParticleIndexer();
    unsigned int n_particles = structural_particle_index->size();
    
    std::vector<int> protein_chain_ids;
    for (unsigned int p = 0; p < n_particles; p++){
        
        SBStructuralParticle* particle = structural_particle_index->getReferenceTarget(p);
        SBAtom* atom = static_cast<SBAtom*>(particle);
        int id = atom->getChainID();
        if (std::find(protein_chain_ids.begin(), protein_chain_ids.end(), id) == protein_chain_ids.end())
        {
            protein_chain_ids.push_back(id);
        }
    }
    return protein_chain_ids;
}

SBInertiaTensor33 SECarbonStateUpdater::computeInertiaTensor(SBPointerIndexer<SBStructuralParticle>* particle_system, SBPosition3& center_of_mass){
    
    SBInertiaTensor33 inertia_tensor;
    SBPointerIndexer<SBStructuralParticle> const* structural_particle_index = (*dynamicalModel)->getStructuralParticleIndexer();
    
    unsigned int n_particles = structural_particle_index->size();
    for (unsigned int i = 0; i < n_particles; i++) {
        SBStructuralParticle* particle = structural_particle_index->getReferenceTarget(i);
        SBAtom* atom = static_cast<SBAtom*>(particle);
        
        if (particle_system->getIndex(particle))   {
            SBPosition3 atom_position = atom->getPosition() - center_of_mass;
            SBQuantity::mass atom_mass = atom->getAtomicWeight();
            inertia_tensor.m[0][0] += atom_mass*(atom_position.v[1]*atom_position.v[1] + atom_position.v[2]*atom_position.v[2]);
            inertia_tensor.m[1][1] += atom_mass*(atom_position.v[0]*atom_position.v[0] + atom_position.v[2]*atom_position.v[2]);
            inertia_tensor.m[2][2] += atom_mass*(atom_position.v[1]*atom_position.v[1] + atom_position.v[0]*atom_position.v[0]);
            inertia_tensor.m[0][1] -= atom_mass*(atom_position.v[0]*atom_position.v[1]);
            inertia_tensor.m[0][2] -= atom_mass*(atom_position.v[0]*atom_position.v[2]);
            inertia_tensor.m[1][0] -= atom_mass*(atom_position.v[1]*atom_position.v[0]);
            inertia_tensor.m[1][2] -= atom_mass*(atom_position.v[1]*atom_position.v[2]);
            inertia_tensor.m[2][0] -= atom_mass*(atom_position.v[2]*atom_position.v[0]);
            inertia_tensor.m[2][1] -= atom_mass*(atom_position.v[2]*atom_position.v[1]);
        }
    }
    
    return inertia_tensor;
}

RigidObject SECarbonStateUpdater::getCurrentRigidObject(int chain_id){
    
    SBPointerIndexer<SBStructuralParticle> const* structural_particle_index = (*dynamicalModel)->getStructuralParticleIndexer();
    unsigned int n_particles = structural_particle_index->size();
    SBPointerIndexer<SBStructuralParticle>* particle_system = new SBPointerIndexer<SBStructuralParticle>;
    
    SBQuantity::mass mass;
    SBDTypePhysicalVector3<SBQuantity::lengthMass> length_mass;;
    
    for (unsigned int i = 0; i < n_particles; i++) {
        
        SBStructuralParticle* particle = structural_particle_index->getReferenceTarget(i);
        
        SBAtom* atom = static_cast<SBAtom*>(particle);
        int id = atom->getChainID();
        if (atom->getChainID() == (chain_id)){
            SBQuantity::mass atom_mass;
            particle_system->addReferenceTarget(atom);
            atom_mass = atom->getAtomicWeight();
            mass += atom_mass;
            length_mass += atom->getPosition()*atom_mass;
        }
    }
    
    if (mass.getValue()==0.0) {
        printf("WARNING! MASS IS ZERO! SETTING IT TO 1 TO AVOID DIVISION BY 0\n");
        mass.setValue(1.0);
    }
    
    RigidObject rigid_body;
    rigid_body.mass_ = mass;
    rigid_body.center_of_mass_ = length_mass/mass;
    rigid_body.q_.w = SBQuantity::dimensionless(1.0);
    rigid_body.q_.x = SBQuantity::dimensionless(0.0);
    rigid_body.q_.y = SBQuantity::dimensionless(0.0);
    rigid_body.q_.z = SBQuantity::dimensionless(0.0);
    rigid_body.particle_system_ = particle_system;
    rigid_body.center_of_mass_original_ = rigid_body.center_of_mass_;
    rigid_body.inertia_tensor_ = computeInertiaTensor(particle_system, rigid_body.center_of_mass_);
    rigid_body.inertia_tensor_original_ = rigid_body.inertia_tensor_;
    rigid_body.inertia_tensor_inverse_ = rigid_body.inertia_tensor_.inverse();
    
    return rigid_body;
}

void SECarbonStateUpdater::updateConditions(){
    
    (*dynamicalModel)->flushPositionBuffer(); // tell the dynamical model that we have used the list of updated positions
    (*dynamicalModel)->updateStructuralNodes(); // update the structural model based on the new positions in the dynamical model
    (*interactionModel)->flushForceBuffer(); // notify the interaction model that we have used the list of updated forces (actually, we used all forces in this non-adaptive algorithm)
    (*interactionModel)->updateInteractions(); // compute the new energy and forces based on the updated positions
    
}

void SECarbonStateUpdater::getSpatialForces(std::vector<SBForce6>& spatial_forces){
    
    unsigned int rigid_body_id = 0;
    for (std::vector<RigidObject>::iterator i = rigid_bodies_->begin(); i != rigid_bodies_->end(); ++i) {
        SBForce6 spatial_force = computeSpatialForce(*i);
        spatial_forces[rigid_body_id] = spatial_force;
        rigid_body_id++;
    }
}

void SECarbonStateUpdater::addRigidObject(){
    
    SBPointerIndexer<SBStructuralParticle> const* structural_particle_index = (*dynamicalModel)->getStructuralParticleIndexer();
    unsigned int n_particles = structural_particle_index->size();
    SBPointerIndexer<SBStructuralParticle>* particle_system = new SBPointerIndexer<SBStructuralParticle>;
    
    SBQuantity::mass mass;
    SBDTypePhysicalVector3<SBQuantity::lengthMass> length_mass;

    int n_selected_atoms = 0;
    
    for (unsigned int i = 0; i < n_particles; i++) {
        
        SBStructuralParticle* particle = structural_particle_index->getReferenceTarget(i);
        
        SBAtom* atom = static_cast<SBAtom*>(particle);
        if (atom->isSelected()){
            SBQuantity::mass atom_mass;
            particle_system->addReferenceTarget(atom);
            atom_mass = atom->getAtomicWeight();
            mass += atom_mass;
            length_mass += atom->getPosition() * atom_mass;
            n_selected_atoms+=1;
        }
    }
    
    RigidObject rigid_body;
    rigid_body.mass_ = mass;
    rigid_body.center_of_mass_ = length_mass/mass;
    rigid_body.q_.w = SBQuantity::dimensionless(1.0);
    rigid_body.q_.x = SBQuantity::dimensionless(0.0);
    rigid_body.q_.y = SBQuantity::dimensionless(0.0);
    rigid_body.q_.z = SBQuantity::dimensionless(0.0);
    rigid_body.particle_system_ = particle_system;
    rigid_body.center_of_mass_original_ = rigid_body.center_of_mass_;
    rigid_body.inertia_tensor_ = computeInertiaTensor(particle_system, rigid_body.center_of_mass_);
    rigid_body.inertia_tensor_original_ = rigid_body.inertia_tensor_;
    rigid_body.inertia_tensor_inverse_ = rigid_body.inertia_tensor_.inverse();
    
    rigid_bodies_->push_back(rigid_body);
    
    printf("Adding rigid body:\nMASS=%8.3f\nATOMS=%8d\n", rigid_body.mass_.getValue(), n_selected_atoms);
    SAMSON::setStatusMessage(QString("rigid body is added"), 0);
    
}

SBQuantity::time SECarbonStateUpdater::getStepSize(SBForce6& spatial_force, RigidObject& rigid_body){
    
    SBQuantity::time dt;
    //    going to solve equation A*t^8 + B*t^4 - C = 0
    //    only one positive solution
    //    determine coefficients A, B and C
    SBQuantityProduct4<SBQuantity::energy, SBQuantity::inverseSquareTime, SBQuantity::inverseSquareTime, SBQuantity::inverseSquareTime>::Type A;
    
    SBQuantityProduct2<SBQuantity::energy, SBQuantity::inverseSquareTime>::Type B;
    
    SBQuantityProduct2<SBQuantity::squareLength, SBQuantity::mass>::Type C;
    
    SBQuantityProduct2<SBQuantity::energy, SBQuantity::inverseSquareTime>::Type sqrt_determinant;
    
    SBQuantityPower<4, SBQuantity::time>::Type dt_power4;
    
    A = pow<2>((rigid_body.inertia_tensor_inverse_.operator *(spatial_force.angular)).norm()) / 16.0 / rigid_body.mass_ * pow<2>(spatial_force.linear.norm());
    
    B = pow<2>(spatial_force.linear.norm()) / 4.0 / rigid_body.mass_;
    B = spatial_force.angular | (rigid_body.inertia_tensor_inverse_.operator *(spatial_force.angular));
    B = pow<2>(max_stepsize_) * pow<2>(rigid_body.inertia_tensor_inverse_.operator *(spatial_force.angular).norm()) / 4.0 * rigid_body.mass_;
    C = rigid_body.mass_ * pow<2>(max_stepsize_);
    
    sqrt_determinant = sqrt(pow<2>(B) + 4.0 * A * C);
    dt_power4 = ((-1.0) * B + sqrt_determinant) / 2 / A;
    
    dt = root<4>(dt_power4);
    return dt;
}